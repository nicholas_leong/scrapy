# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

def serialize_date(value):
    return '$ %s' % str(value)

#class ScraperMetaData(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #title=Field()
    #author=Field()
    #date=Field()

class ScraperImage(Item):
    title=Field()
    image_urls=Field()
    images=Field()
