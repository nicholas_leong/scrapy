#!/usr/bin/python
# -*- coding: "utf-8" -*-
import sys
import scrapy
import scrapy.http
from scrapy.exporters import XmlItemExporter
from scrapy.spiders import BaseSpider
from scrapy.selector import Selector
from scrapy.selector import HtmlXPathSelector
from scrapy.http import HtmlResponse
from scrapy.http import XmlResponse
#from tutorial0.items import ScraperMetaData
#from scraper0.items import ScraperImage
#from bs4 import BeautifulSoup
import json
import pprint
import urlparse

class scraper0Spider(scrapy.Spider):
    reload(sys)  # #hacks
    sys.setdefaultencoding('UTF8')
    name = "scraper0"
    allowed_domains = ["localhost"]
    start_urls = [
       "http://localhost/"
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]

    def start_requests(self):
         for i in xrange(1,11):
             yield scrapy.Request("http://localhost/article%s.html" % i, self.parse, meta={"index": i})

         #yield scrapy.Request("http://localhost/article1.html", self.parse)




    def parse(self,response):
        sel = Selector(response)
        #hxs = HtmlXPathSelector(response)
        #print "http://localhost/article%s.html" % str(i)

        charsetlist=response.xpath('/html/head/meta[@http-equiv="Content-Type"]/@content').extract()
        charsetstring = unicode(''.join(charsetlist))
        print "Charset: " + charsetstring

        authorlist=response.xpath('//meta[@name="author"]/@content').extract()
        #authorstring = unicode(''.join(authorlist))
        authorstring = ''.join(authorlist)
        print "Author: " + authorstring

        authorlist0=response.xpath('//meta[@name="ERPTOC_AUTHOR"]/@content').extract()
        authorstring0 = ''.join(authorlist0)
        print "ERPTOC_AUTHOR: " + authorstring0

        datelist=response.xpath('//meta[@name="ERPTOC_DATE"]/@content').extract()
        datestring = unicode(''.join(datelist))
        print "ERPTOC_DATE: " + datestring

        titlelist=response.xpath('//meta[@name="ERPTOC_TITLE"]/@content').extract()
        titlestring = unicode(''.join(titlelist))
        print "ERPTOC_TITLE: " + titlestring

        topiclist=response.xpath('//meta[@name="ERPTOC_TOPIC"]/@content').extract()
        topicstring = unicode(''.join(topiclist))
        print "ERPTOC_TOPIC: " + topicstring

        regionlist=response.xpath('//meta[@name="ERPTOC_REGION"]/@content').extract()
        regionstring = unicode(''.join(regionlist))
        print "ERPTOC_REGION: " + regionstring

        issuelist=response.xpath('//p[@class="titletext"]/span/text()').extract()
        issuestring = unicode(''.join(issuelist))
        print "Issue #: " + issuestring

        #metadata = ScraperMetaData (title=titlestring, author=authorstring0, date=datestring)
        #list=response.xpath('//p[@class="articletext"]/font/text()').extract()
        #list=response.xpath('//p[@class="articletext"]/font/b/text()').extract()
        #list0=response.xpath('//p[@class="titletext"]/text()').extract()
        #list0+=response.xpath('//p[@class="titletext"]/span/text()').extract()
        contentlist=response.xpath('//child::p[@class="articletext"]//text()').extract()
        captionlist=response.xpath('//child::div//text()').extract()
        captionstring = unicode(', '.join(captionlist))
        #print captionlist
        #imagelist=response.xpath('//div/img/@src').extract()
        imagelist=response.xpath('//img/@src').extract()
        imagestring = unicode(', '.join(imagelist))
        print "Image string:" + imagestring
        print "Image count:" + str(len(imagelist))

        #hxs = HtmlXPathSelector(response)
        #items = []
        #images=hxs.select('//a[@class="image"]')
        #for image in images:
                #item = ScraperImage()
                #image_urls = hxs.select('//img/@src').extract()
                #item['image_urls'] = ["http:" + x for x in image_urls]
                #return item

        #print len(list)
        #pprint.pprint(list)
        #for x in xrange(0,len(list)):
            #bulletpoint = "<b>" + unicode(x+1) + ")" + "</b>"
            #list[x]=bulletpoint+list[x]

        #teststring = ''.join(testlist).encode('utf-8').rstrip('\r\n\t')
        #print "teststring: " + teststring
        #string1 = ''.join(list1).encode('utf-8')
        #string0 = ''.join(list0)
        #string = ''.join(list)
        #string = ''.join(c for c in list if c not in '\r\t\n').encode('utf-8')
        #string0 = ''.join(c for c in list if c not in '\r\t\n').encode('utf-8')
        #string1 = ''.join(list1).encode('utf-8')


        #for i in list:
            #list[i]="<p>" + list[i] + "</p>"
        #list[0]="<p>" + list[0] + "</p>"
        for k in xrange(0,len(contentlist)):
            #print "contentlist[k] type:" + str(type(contentlist[k]))
            contentlist[k]="<p>" + contentlist[k] + "</p>"
            contentlist[k]=unicode(contentlist[k])
            #contentlist[k]=contentlist[k].encode('latin_1')


        #contentstring = ''.join(contentlist).encode('utf-8')
        #contentstring = ''.join(c for c in contentlist if c not in '\r\t\n')

        #contentstring = ' '.join(' '.join(contentlist).split()).decode('iso-8859-1').encode('utf8')
        contentstring = ' '.join(' '.join(contentlist).split())
        #contentstring = contentstring.decode('utf-8').encode('latin-1')
        contentstring = contentstring.decode('latin-1').encode('latin-1')

        #fullstring = string0 + string
        #fullstring = fullstring.encode('utf-8')
        #contentstring = contentstring.replace('\r\n              \t', '')
        #contentstring = contentstring.replace("\r\n                ", "")
        #contentstring = contentstring.replace("\r\n\r\n", '')
        #contentstring = contentstring.replace("\r\n\r\n", '')
        #contentstring = contentstring.replace("\r", '')
        #contentstring = contentstring.replace("\n", '')
        #contentstring = contentstring.replace("\s+", '')
        #contentstring = contentstring.replace("\t", '')
        #contentstring = contentstring.replace("\u0097", '')
        #contentstring = contentstring.replace("\u00a0", '')
        #captionstring = captionstring.replace("\u00a0", '')
        fullstring = {"title":titlestring, "content":contentstring, "author":authorstring, "erptoc_author":authorstring0,
        "date": datestring ,"issue": issuestring ,"region": regionstring, "captions": captionstring, "imageurls": imagestring
        }
        #print type(fullstring)

        #stringjson=json.dumps(contentstring)
        with open("output%s.json" % response.meta["index"], 'w') as f:
        #with open("article1out.json", 'w') as f:
            json.dump(fullstring, f, ensure_ascii=False)

        #with open("output%s.txt" % response.meta["index"], 'w') as text_file:
        #with open("article1out.txt", 'w') as text_file:
            #text_file.write(contentstring)
            #text_file.close()

        with open("charset.txt", 'ab') as text_file:
            text_file.write(charsetstring)
            text_file.close()

        #try:
            #json.loads(stringjson)
            #print("Valid JSON")
        #except ValueError:
            #print("Not valid JSON")
